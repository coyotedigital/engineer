#Software Engineering Code Test

We would like you to begin refactoring the ‘AddCustomer’ method in the CustomerService class in order to make the class easier to maintain. During the refactoring process you should consider the SOLID principles, how readable the code is and where unit tests might be appropriate.

You can use whatever test frameworks you wish and have two hours to get as far as you can. We’re not expecting the work to be ‘finished’, but you should have started the work and be able to discuss the changes you have made and what your next steps would be. Please attempt at least some refactoring and unit testing to demonstrate your skills in both.

We are aware that the validation logic isn't perfect but we do not expect you to fix that or make it more comprehensive.

You can change anything (method signatures, constructors etc.) apart from making the CustomerDataAccess class and it methods non static.

Feel free to use the internet to look up anything you need.

In the discussion following we will expect you to explain the changes you have made so far and what your next steps would be.